/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 15.384615384615385, "KoPercent": 84.61538461538461};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.15384615384615385, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "Get detail employees"], "isController": false}, {"data": [0.0, 500, 1500, "Post Register"], "isController": false}, {"data": [0.0, 500, 1500, "Get grades"], "isController": false}, {"data": [0.0, 500, 1500, "Put update employees"], "isController": false}, {"data": [1.0, 500, 1500, "Get direktur"], "isController": false}, {"data": [0.0, 500, 1500, "Get employees"], "isController": false}, {"data": [0.0, 500, 1500, "Post employees"], "isController": false}, {"data": [1.0, 500, 1500, "Get Dashboard"], "isController": false}, {"data": [0.0, 500, 1500, "Delete employees id"], "isController": false}, {"data": [0.0, 500, 1500, "Post change password"], "isController": false}, {"data": [0.0, 500, 1500, "Get level"], "isController": false}, {"data": [0.0, 500, 1500, "Post Login"], "isController": false}, {"data": [0.0, 500, 1500, "Post forgot"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 52, 44, 84.61538461538461, 390.5961538461538, 336, 1412, 344.0, 405.7, 742.5999999999947, 1412.0, 3.4375619752759965, 2.4420841045812125, 1.0011589376611356], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Get detail employees", 4, 4, 100.0, 343.0, 341, 344, 343.5, 344.0, 344.0, 344.0, 0.4104669061056952, 0.22607747562852748, 0.07094984607491021], "isController": false}, {"data": ["Post Register", 4, 4, 100.0, 861.5, 336, 1412, 849.0, 1412.0, 1412.0, 1412.0, 0.3675119441381845, 0.3082937109518559, 0.09439027471517825], "isController": false}, {"data": ["Get grades", 4, 4, 100.0, 340.0, 336, 344, 340.0, 344.0, 344.0, 344.0, 0.40845501889104463, 0.23175035739814154, 0.07738307975084245], "isController": false}, {"data": ["Put update employees", 4, 4, 100.0, 342.25, 338, 347, 342.0, 347.0, 347.0, 347.0, 0.4106776180698152, 0.2261935318275154, 0.22298511293634496], "isController": false}, {"data": ["Get direktur", 4, 0, 0.0, 404.75, 399, 409, 405.5, 409.0, 409.0, 409.0, 0.4056383733901227, 0.8231606835006591, 0.07803785113071696], "isController": false}, {"data": ["Get employees", 4, 4, 100.0, 351.25, 336, 375, 347.0, 375.0, 375.0, 375.0, 0.4104669061056952, 0.2252757824525398, 0.07014815289892253], "isController": false}, {"data": ["Post employees", 4, 4, 100.0, 346.5, 338, 359, 344.5, 359.0, 359.0, 359.0, 0.4105933073290905, 0.2253451549989735, 0.22334030486553066], "isController": false}, {"data": ["Get Dashboard", 4, 0, 0.0, 345.5, 343, 350, 344.5, 350.0, 350.0, 350.0, 0.407955124936257, 0.23066993880673126, 0.07489801121876594], "isController": false}, {"data": ["Delete employees id", 4, 4, 100.0, 359.5, 336, 415, 343.5, 415.0, 415.0, 415.0, 0.407705636530425, 0.22455662012027316, 0.22256586994190194], "isController": false}, {"data": ["Post change password", 4, 4, 100.0, 346.75, 345, 349, 346.5, 349.0, 349.0, 349.0, 0.4078719282145406, 0.2772254512083206, 0.12347685326807382], "isController": false}, {"data": ["Get level", 4, 4, 100.0, 349.75, 338, 379, 341.0, 379.0, 379.0, 379.0, 0.4086636697997548, 0.23865319779321617, 0.07502809562729873], "isController": false}, {"data": ["Post Login", 4, 4, 100.0, 341.75, 338, 346, 341.5, 346.0, 346.0, 346.0, 0.4077887654195127, 0.22062009379141606, 0.10393834743602814], "isController": false}, {"data": ["Post forgot", 4, 4, 100.0, 345.25, 341, 349, 345.5, 349.0, 349.0, 349.0, 0.40774719673802245, 0.27714067278287463, 0.1003440366972477], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400", 8, 18.181818181818183, 15.384615384615385], "isController": false}, {"data": ["401", 4, 9.090909090909092, 7.6923076923076925], "isController": false}, {"data": ["500", 8, 18.181818181818183, 15.384615384615385], "isController": false}, {"data": ["404", 24, 54.54545454545455, 46.15384615384615], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 52, 44, "404", 24, "400", 8, "500", 8, "401", 4, "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["Get detail employees", 4, 4, "404", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Post Register", 4, 4, "400", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get grades", 4, 4, "404", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Put update employees", 4, 4, "404", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["Get employees", 4, 4, "404", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Post employees", 4, 4, "404", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["Delete employees id", 4, 4, "404", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Post change password", 4, 4, "500", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Get level", 4, 4, "400", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Post Login", 4, 4, "401", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["Post forgot", 4, 4, "500", 4, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
